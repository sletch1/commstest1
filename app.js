function sendMessage() {
    const input = document.getElementById('messageInput');
    fetch('/send-message', {
       method: 'POST',
       headers: {
          'Content-Type': 'application/json',
       },
       body: JSON.stringify({ message: input.value })
    }).then(() => {
       input.value = '';
       console.log('Message sent!');
    });
 }
 
// Import necessary libraries
const express = require('express');
const http = require('http');
const { MongoClient } = require('mongodb');
const socketIo = require('socket.io');

// Initialize Express and HTTP server
const app = express();
const server = http.createServer(app);

// Define the port number
const PORT = 3000;

// Serve static files from the 'public' directory
app.use(express.static('public'));

// Basic route to confirm the server is working
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
});

// MongoDB connection string for a local database
// Replace 'yourDatabaseName' with the actual name of your database
const uri = "mongodb://localhost:27017/test";
const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });

// Function to connect to the database
async function connectDB() {
    try {
        await client.connect();
        console.log("Connected to database!");
    } catch (e) {
        console.error("Failed to connect to the database:", e);
    }
}

// Connect to MongoDB
connectDB();

// Initialize Socket.io for real-time communication
const io = socketIo(server);

// Handle socket connection
io.on('connection', (socket) => {
    console.log('A user connected');
    socket.on('chat message', (msg) => {
        io.emit('chat message', msg);  // Emit the received message to all clients
    });

    // Handle socket disconnection
    socket.on('disconnect', () => {
        console.log('A user disconnected');
    });
});

// Start the server and handle any potential server errors
server.listen(PORT, () => {
    console.log(`Server running on http://localhost:${PORT}`);
}).on('error', (error) => {
    console.error('Error occurred while starting the server:', error);
});
